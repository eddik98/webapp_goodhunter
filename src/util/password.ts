import * as crypto from 'crypto';
import { PasswordDataDto } from './dto/password-data.dto';

export class Password {

  static generatePasswordData(password: string): PasswordDataDto {
    const salt = crypto.randomBytes(128).toString('base64');
    const password_hash = crypto
      .pbkdf2Sync(password, salt, 1, 128, 'sha1')
      .toString('base64');
    return {salt, password_hash};
  }

  static getPasswordHash(password: string, salt: string): PasswordDataDto {
    const password_hash = crypto
      .pbkdf2Sync(password, salt, 1, 128, 'sha1')
      .toString('base64');

    return {password_hash};
  }

}