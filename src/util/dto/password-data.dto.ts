export class PasswordDataDto {
  readonly salt?: string;
  readonly password_hash: string;
}