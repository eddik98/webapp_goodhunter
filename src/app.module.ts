import { TypesModule } from './types/types.module';
import { Module } from '@nestjs/common';
import { UserModule} from './user';
import { AuthModule } from './auth';
import { GroupModule } from './group/group.module';
import { RoleModule } from './role/role.module';
import { UserGroupModule } from './userGroup/userGroup.module';
import { RoleGroupModule } from './roleGroup/roleGroup.module';
import { DatabaseModule } from './common/database/database.module';
import { SessionModule } from './session/session.module';
import { ComparatorModule } from './comparator/comparator.module';
import { CategorySchemaModule } from './categorySchema';
import { PriceItemModule } from './priceItem/priceItem.module';
import { PriceModule } from './price/price.module';
import { SitesModule } from './sites/sites.module';
import { RouteModule } from './route/route.module';
import { RouteGroupModule } from './routeGroup/routeGroup.module';

@Module({
  imports: [
		RouteGroupModule,
		RouteModule,
    RoleModule,
    UserModule,
    AuthModule,
    GroupModule,
    UserGroupModule,
    RoleGroupModule,
    DatabaseModule,
    SessionModule,
    ComparatorModule,
    CategorySchemaModule,
    PriceItemModule,
    PriceModule,
		TypesModule,
		SitesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}