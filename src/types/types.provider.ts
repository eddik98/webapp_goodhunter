import { Types } from './model/Types';
import * as Constants from '../util/constants';

export const typesProviders = [{
  provide: Constants.TYPES_REPOSITORY,
  useValue: Types,
}];