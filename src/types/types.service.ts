import { Inject, Injectable } from '@nestjs/common';
import { Types } from './model/Types';
import * as Constants from '../util/constants';
import { BaseService } from '../common/BaseService';

@Injectable()
export class TypesService extends BaseService<Types>{
  constructor(
  	@Inject(Constants.TYPES_REPOSITORY)
		private readonly typesRepository: typeof Types)
	{super(Types); }

}