import { Module } from '@nestjs/common';
import { TypesService } from './types.service';
import { typesProviders } from './types.provider';

@Module({
  imports: [],
  controllers: [],
  providers: [TypesService, ...typesProviders],
  exports: [TypesService],
})
export class TypesModule {}