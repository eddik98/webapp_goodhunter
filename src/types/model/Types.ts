import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType, HasMany } from 'sequelize-typescript';
import { PriceItem } from '../../priceItem/model/PriceItem';

@Table({
  timestamps: false,
  tableName: 'types',
})
export class Types extends Model<Types> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  title: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

	@HasMany(() => PriceItem)
	price_item: PriceItem[];
}
