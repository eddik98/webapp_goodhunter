import { PriceItem } from './model/PriceItem';
import * as Constants from '../util/constants';

export const priceItemProviders = [{
    provide: Constants.PRICE_ITEM_REPOSITORY,
    useValue: PriceItem,
}];