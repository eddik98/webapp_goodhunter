import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty} from 'class-validator';

export class PriceItemDataDto {
	@ApiModelProperty()
	@IsNotEmpty()
	readonly price_id: number;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly name: string;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly price: number;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly brand: string;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly model: string;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly color: string;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly diagonal: string;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly memory: string;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly type_id: number;
}