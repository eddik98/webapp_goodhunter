import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
export class PriceIdItemDto {
	@ApiModelProperty()
	@IsNotEmpty()
	readonly price_id: number;
}