import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
export class PriceItemIdDto {
	@ApiModelProperty()
	@IsNotEmpty()
	readonly id: number;
}