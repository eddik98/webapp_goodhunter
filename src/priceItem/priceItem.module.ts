import { Module } from '@nestjs/common';
import { PriceItemService } from './priceItem.service';
import { priceItemProviders } from './priceItem.providers';

@Module({
  imports: [],
  controllers: [],
  providers: [PriceItemService, ...priceItemProviders],
  exports: [PriceItemService],
})
export class PriceItemModule {}