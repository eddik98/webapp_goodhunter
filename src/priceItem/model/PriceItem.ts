import { Table, Column, Model, AllowNull, ForeignKey, PrimaryKey, AutoIncrement, DataType} from 'sequelize-typescript';
import {Price} from '../../price/model/Price';
import { Types } from '../../types/model/Types';

@Table({
  timestamps: false,
  tableName: 'price_item',
})
export class PriceItem extends Model<PriceItem> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @ForeignKey(() => Price)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  price_id: number;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  price: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  brand: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  model: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  color: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  diagonal: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  memory: string;

	@ForeignKey(() => Types)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  type_id: number;
}
