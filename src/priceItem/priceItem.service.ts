import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../common/BaseService';
import { PriceItem } from './model/PriceItem';
import * as Constants from '../util/constants';

@Injectable()
export class PriceItemService extends BaseService<PriceItem>{
  constructor(
  	@Inject(Constants.PRICE_ITEM_REPOSITORY)
		private readonly priceItemRepository: typeof PriceItem,
	) {super(PriceItem); }
}