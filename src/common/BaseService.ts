import { Model } from 'sequelize-typescript';
import { InternalServerErrorException } from '@nestjs/common';

type NonAbstract<T> = { [P in keyof T]: T[P] };
type Constructor<T> = (new () => T);
type NonAbstractTypeOfModel<T> = Constructor<T> & NonAbstract<typeof Model>;

export class BaseService<T extends Model<T>> {

	constructor(private entityType: NonAbstractTypeOfModel<T>) {}

	public async findOne(where: object, attributes: string[] = []): Promise<T> {
		try {
			const params = {where};

			if (attributes.length !== 0) {
				params[`attributes`] = attributes;
			}

			const result = await this.entityType.findOne<T>(params);

			if (result !== null) {
				return result.toJSON() as T;
			}

			return result;
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}

	public async findAll(where: object, attributes: string[] = [], onMap?: boolean): Promise<any[]> {
		try {
			const params = {where};

			if (attributes.length !== 0) {
				params[`attributes`] = attributes;
			}

			const result = await this.entityType.findAll<T>(params);

			if (result.length === 0) {
				return null;
			}

			if (onMap === true) {
				return result.map((value => value.dataValues));
			}

			return result;
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}

	public async create(values: object, ...additionalValues: object[]): Promise<T> {
		try {
			if (additionalValues !== undefined) {
				values = Object.assign({}, values, ...additionalValues);
			}

			return await this.entityType.create<T>(values);
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}

	public async update(keys: object, whereParam: object = {}): Promise<[number, Array<T>]> {
		try {
			let whereValue = {};

			if (whereParam !== {}) {
				whereValue = whereParam;
			}

			return await this.entityType.update<T>(keys, {where: whereValue});
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}

	public async delete(values: object, ...additionalValues: object[]): Promise<boolean> {
		try {
			if (additionalValues !== undefined) {
				values = Object.assign({}, values, ...additionalValues);
			}

			values = {where: values};

			await this.entityType.destroy(values);

			return true;
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}

	public map<K>(values: Partial<K>, ctor: new () => K): K {
		const instance = new ctor();

		return Object.keys(instance).reduce((acc, key) => {
			acc[key] = values[key];
			return acc;
		}, {}) as K;
	}
}