import {
	Body,
	Catch,
	Controller,
	HttpException,
	HttpStatus,
	Get,
	Res,
	UseFilters,
	Query,
	UseGuards,
	UseInterceptors,
	Param,
	Post,
	Delete,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiBadRequestResponse,
	ApiCreatedResponse,
	ApiResponse,
	ApiNotFoundResponse,
	ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import { ApiException } from './api-exception.model';

@ApiBadRequestResponse({
	type: ApiException,
	description: 'Wrong data.',
})
@ApiNotFoundResponse({
	type: ApiException,
	description: 'Record(s) not found.',
})
@ApiInternalServerErrorResponse({
	type: ApiException,
	description: 'An error has occurred on the server. Apologize.',
})
export class BaseController {
	constructor() {}
}