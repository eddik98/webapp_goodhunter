import { HttpException, HttpStatus } from '@nestjs/common';

export class HandlerAllException extends Error {
  public status: number;
  public message: any;

  constructor(m: any) {
    super(m);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, HandlerAllException.prototype);

    this.status = HttpStatus.INTERNAL_SERVER_ERROR;

    if (m.status !== undefined){
      this.status = m.status;
    }

    global.console.error(m);
  }

  public getStatus(): number {
    return this.status;
  }

}
