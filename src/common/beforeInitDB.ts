import { Sequelize } from 'sequelize-typescript';
import { saveRoles } from '../role/beforeInitDB/roles';
import { saveGroups, saveRelationshipRolesGroup } from '../roleGroup/beforeInitDB/rolesGroup';
import { saveRoutes } from '../route/beforeInitDB/routes';

export async function beforeInitDB(sequelize: Sequelize) {
	await sequelize.transaction(async (t) => {
		const options = {raw: true, transaction: t};
		await sequelize.query('SET FOREIGN_KEY_CHECKS = 0', options);
		await sequelize.query('truncate table role', options);
		await sequelize.query('SET FOREIGN_KEY_CHECKS = 1', options);
	});

	await saveRoles();

	await saveGroups();

	await saveRelationshipRolesGroup();

	await saveRoutes();
}