import { Sequelize } from 'sequelize-typescript';
import { Role } from '../../role/model/Role';
import { User } from '../../user/model/User';
import { CategorySchema } from '../../categorySchema/model/CategorySchema';
import { Price } from '../../price/model/Price';
import { Session } from '../../session/model/Session';
import { UserGroup } from '../../userGroup/model/UserGroup';
import { RoleGroup } from '../../roleGroup/model/RoleGroup';
import { Group } from '../../group/model/Group';
import { Types } from '../../types/model/Types';
import { PriceItem } from '../../priceItem/model/PriceItem';
import { UserSites } from '../../sites/model/UserSites';
import { Route } from '../../route/model/Route';
import { RouteGroup } from '../../routeGroup/model/RouteGroup';
import { beforeInitDB } from '../beforeInitDB';

export const databaseProviders =
	[{
		provide: 'SequelizeToken',
		useFactory: async () => {
			const sequelize = new Sequelize({
				operatorsAliases: Sequelize.Op as any,
				dialect: 'mysql',
				host: 'localhost',
				port: 3306,
				database: 'ghBackEndTest',
				username: 'root',
				password: '989898',
				modelPaths: [__dirname + '/entity'],
			});

			sequelize.addModels([
				Route,
				RouteGroup,
				User,
				CategorySchema,
				Price,
				Session,
				UserGroup,
				RoleGroup,
				Role,
				Group,
				Types,
				PriceItem,
				UserSites,
			]);

			// await sequelize.sync({force: true});
			await sequelize.sync();
			await beforeInitDB(sequelize);
			return sequelize;
		},
	}];