import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType } from 'sequelize-typescript';

@Table({
  timestamps: false,
  tableName: 'product',
})
export class Goods extends Model<Goods> {

  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  provider_id: number;
}
