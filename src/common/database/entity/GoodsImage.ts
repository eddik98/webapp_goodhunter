import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType } from 'sequelize-typescript';

@Table({
  timestamps: false,
  tableName: 'goods_image',
})
export class GoodsImage extends Model<GoodsImage> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  product_id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  link: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  cached: string;
}
