import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Roles } from '../../util/roles';

@Injectable()
export class RolesGuard implements CanActivate {
  requiredRoles: any[];

  constructor(...args: Roles[]) {
    this.requiredRoles = args;
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
      const request = context.switchToHttp().getRequest();
      const userReq = request.user;

      this.checkRoles(this.requiredRoles, userReq.roles);

      return true;
    } catch (e) {
      return false;
    }
  }

  checkRoles(requiredRoles: any[], userRoles: any[]){
    let counter = 0;

    for (const item of this.requiredRoles) {
      if (userRoles.indexOf(item) !== -1) {
        counter++;
      }
    }

    if (counter === 0){
      throw new Error();
    }
  }
}