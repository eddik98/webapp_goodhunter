import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../common/BaseService';
import { Route } from './model/Route';
import * as Constants from '../util/constants';

@Injectable()
export class RouteService extends BaseService<Route>{
  constructor(
  	@Inject(Constants.ROUTE_REPOSITORY)
		private readonly routeRepository: typeof Route,
	) {super(Route); }
}