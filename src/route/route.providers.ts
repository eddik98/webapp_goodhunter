import { Route } from './model/Route';
import * as Constants from '../util/constants';

export const routeProviders = [{
    provide: Constants.ROUTE_REPOSITORY,
    useValue: Route,
}];