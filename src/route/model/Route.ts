import {
	Table,
	Column,
	Model,
	AllowNull,
	PrimaryKey,
	AutoIncrement,
	DataType, HasMany,
} from 'sequelize-typescript';
import { UserSites } from '../../sites/model/UserSites';
import { RouteGroup } from '../../routeGroup/model/RouteGroup';

@Table({
  timestamps: false,
  tableName: 'route',
})
export class Route extends Model<Route> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  url: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  method: string;

  @AllowNull(true)
  @Column({type: DataType.STRING})
	description: string;

	@HasMany(() => RouteGroup)
	route_group: RouteGroup[];
}
