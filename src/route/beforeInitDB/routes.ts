import { Route } from '../model/Route';
const swaggerDoc = require('../../../swagger-spec.json');

export async function saveRoutes() {
	try {
		const paths = swaggerDoc.paths;

		for (const name in paths) {
			const pathsValue = paths[name];

			for (const nameMethod in pathsValue) {

				const tmpName =
					name.replace(/\/{/gi, '/:')
						.replace((/}/gi), '');

				const data = {
					url: tmpName,
					method: nameMethod,
					description: pathsValue[nameMethod].summary,
				};
				await Route.findOrCreate({
					where: data,
					defaults: data,
				});
			}
		}
	} catch (e) {
		throw new Error(e);
	}
}