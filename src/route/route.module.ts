import { Module } from '@nestjs/common';
import { RouteService } from './route.service';
import { routeProviders } from './route.providers';

@Module({
  imports: [],
  controllers: [],
  providers: [RouteService, ...routeProviders],
  exports: [RouteService],
})
export class RouteModule {}