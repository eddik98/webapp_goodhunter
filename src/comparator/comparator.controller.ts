import {
  Controller,
  Post,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
} from '@nestjs/swagger';
import { BaseController } from '../common/BaseController';

@ApiUseTags('comparator')
@Controller('comparator')
export class ComparatorController extends BaseController{
  constructor()
	{super(); }

  @Post()
  @ApiOperation({title: ''})
  async findSessionsUser() {
  }
}