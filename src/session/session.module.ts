import { Module } from '@nestjs/common';
import { SessionService } from './session.service';
import { sessionProviders } from './session.provider';

@Module({
  imports: [],
  controllers: [],
  providers: [SessionService, ...sessionProviders],
  exports: [SessionService],
})
export class SessionModule {}