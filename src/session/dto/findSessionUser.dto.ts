import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class FindSessionUserDto {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsNumber()
  readonly user_id: number;
}