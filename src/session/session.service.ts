import { Inject, Injectable } from '@nestjs/common';
import { Session } from './model/Session';
import * as Constants from '../util/constants';
import { BaseService } from '../common/BaseService';

@Injectable()
export class SessionService extends BaseService<Session> {
	constructor(
		@Inject(Constants.SESSION_REPOSITORY)
		private readonly sessionRepository?: typeof Session,
	) {super(Session); }
}