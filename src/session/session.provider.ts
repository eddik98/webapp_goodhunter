import { Session } from './model/Session';
import * as Constants from '../util/constants';

export const sessionProviders = [{
  provide: Constants.SESSION_REPOSITORY,
  useValue: Session,
}];