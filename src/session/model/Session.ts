import { Table, Column, Model, AllowNull, CreatedAt, UpdatedAt, DataType, ForeignKey, PrimaryKey, AutoIncrement } from 'sequelize-typescript';
import { Price } from '../../price/model/Price';
import { User } from '../../user/model/User';

@Table({
  timestamps: true,
  tableName: 'session',
})
export class Session extends Model<Session> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.ENUM('working', 'complete')})
  status: string;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  total: number;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  processed: number;

  @ForeignKey(() => Price)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  price_id: number;

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  user_id: number;

  @CreatedAt
  @Column({type: DataType.DATE})
  created_at: Date;

  @UpdatedAt
  @Column({type: DataType.DATE})
  updated_at: Date;
}
