import { User } from './model/User';
import * as Constants from '../util/constants';

export const userProviders = [
  {
    provide: Constants.USER_REPOSITORY,
    useValue: User,
  },
];