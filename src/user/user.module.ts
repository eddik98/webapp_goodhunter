import { Module, forwardRef} from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { userProviders } from './user.providers';
import { UserGroupModule } from '../userGroup/userGroup.module';
import { AuthModule } from '../auth';
import { UserPriceController } from './otherControllers/user.price.controller';
import { PriceModule } from '../price/price.module';
import { UserSessionController } from './otherControllers/user.session.controller';
import { SessionModule } from '../session/session.module';
import { UserCategorySchemaController } from './otherControllers/user.categorySchema.controller';
import { CategorySchemaModule } from '../categorySchema';

@Module({
  imports: [
  	UserGroupModule,
		forwardRef(() => AuthModule),
		PriceModule,
		SessionModule,
		CategorySchemaModule,
	],
  controllers: [
  	UserController,
		UserPriceController,
		UserSessionController,
		UserCategorySchemaController,
	],
  providers: [
  	UserService,
		...userProviders,
	],
  exports: [UserService],
})
export class UserModule {}