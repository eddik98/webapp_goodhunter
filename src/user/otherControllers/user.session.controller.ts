import {
	Controller,
	Get,
	Param,
	BadRequestException,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiCreatedResponse,
} from '@nestjs/swagger';
import { SessionService } from '../../session/session.service';
import { UserIdDto } from '../dto/user-id.dto';
import { PriceDataDto } from '../../price/dto/price-data.dto';
import { BaseController } from '../../common/BaseController';

@ApiUseTags('user/:user_id/session')
@Controller('user/:user_id/session')
export class UserSessionController extends BaseController {
	constructor(private readonly sessionService: SessionService) {super(); }

	@Get()
	@ApiCreatedResponse({type: PriceDataDto})
	@ApiOperation({title: 'Find all session user items'})
	async findAll(@Param() params: UserIdDto) {
		const result = await this.sessionService.findAll({
			user_id: params.user_id,
		});

		if (result === null) {
			throw new BadRequestException(
				'User width such user_id' +
				' already exists or empty session!',
			);
		}

		return result;
	}
}