import {
	Body,
	Controller,
	Delete,
	Get,
	HttpStatus,
	NotFoundException,
	Param,
	Post,
	Res,
	UseGuards,
} from '@nestjs/common';
import {
	ApiBearerAuth,
	ApiCreatedResponse,
	ApiOperation,
	ApiUseTags,
} from '@nestjs/swagger';
import { PriceService } from '../../price/price.service';
import { FindAllDto } from '../../price/dto/request/find-all.dto';
import { PriceDataDto } from '../../price/dto/price-data.dto';
import { UserIdDto } from '../dto/user-id.dto';
import { JwtGroupsAuthGuard } from '../../auth/guards/jwt-groups-auth.guard';
import { BaseController } from '../../common/BaseController';

@ApiUseTags('user/:user_id/price')
@Controller('user/:user_id/price')
@ApiBearerAuth()
@UseGuards(JwtGroupsAuthGuard)
export class UserPriceController extends BaseController {
  constructor(private readonly priceService: PriceService)
	{super(); }

  @Get()
  @ApiCreatedResponse({type: PriceDataDto})
  @ApiOperation({title: 'Get all user price'})
  async findAll(@Param() param: FindAllDto): Promise<PriceDataDto[]> {
    const result = await this.priceService.findAll(
      param,
      ['name', 'schema_id', 'user_id'],
    );

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }

  @Post()
  @ApiCreatedResponse({type: PriceDataDto})
  @ApiOperation({title: 'Create price'})
  async create(@Param() id: UserIdDto, @Body() param: PriceDataDto): Promise<PriceDataDto> {
    return await this.priceService.create(Object.assign({}, id, param));
  }

  @Delete()
	@ApiCreatedResponse({type: HttpStatus.OK})
  @ApiOperation({title: 'Delete all price'})
  async deleteAll(@Param() param: UserIdDto, @Res() res): Promise<any> {
    await this.priceService.delete({user_id: param.user_id});
    res.status(HttpStatus.OK).send();
  }
}