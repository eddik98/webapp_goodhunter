import {
	Controller,
	Get,
	NotFoundException,
	Param,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiCreatedResponse,
} from '@nestjs/swagger';
import { CategorySchemaService } from '../../categorySchema/index';
import { GetSchemaDto } from '../../categorySchema/dto/request/get-schema.dto';
import { SchemaDataDto } from '../../categorySchema/dto/response/schema-data.dto';
import { BaseController } from '../../common/BaseController';

@ApiUseTags('user/:user_id/schemas')
@Controller('user/:user_id/schemas')
export class UserCategorySchemaController extends BaseController{
	constructor(private readonly categorySchemaService: CategorySchemaService)
	{super(); }

	@Get()
	@ApiCreatedResponse({type: SchemaDataDto})
	@ApiOperation({title: 'Get user schema'})
	async get(@Param() getSchemaDto: GetSchemaDto): Promise<SchemaDataDto> {

		const schema = await this.categorySchemaService.findOne({
			user_id: getSchemaDto.user_id,
		});

		if (schema === null) {
			throw new NotFoundException('Schema not found!');
		}

		return this.categorySchemaService.map(schema, SchemaDataDto);
	}

}