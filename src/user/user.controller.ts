import {
	BadRequestException,
	Body,
	Controller,
	Post,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiUseTags,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import { CreateUserDto } from './dto/request/create-user.dto';
import { RegistrationDto } from './dto/response/registration.dto';
import { UserService } from './user.service';
import { BaseController } from '../common/BaseController';

@ApiUseTags('user')
@Controller('user')
export class UserController extends BaseController{
  constructor(private readonly userService: UserService)
	{super(); }

  @Post()
  @ApiCreatedResponse({type: RegistrationDto})
  @ApiOperation({title: 'Registration user'})
  async registration(@Body() createUserDto: CreateUserDto): Promise<RegistrationDto> {

    if (await this.userService.checkEmail(createUserDto.email)) {
      throw new BadRequestException('E-mail already exist!');
    }

    const user = await this.userService.registration(createUserDto);
    const tokens = await this.userService.authService.createTokens('', user);

    return {
      id: user.id,
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name,
      tokens,
    };
  }
}