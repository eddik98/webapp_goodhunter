import { Table, Column, Model, AllowNull, HasMany, PrimaryKey, AutoIncrement, DataType } from 'sequelize-typescript';
import { Price } from '../../price/model/Price';
import { CategorySchema } from '../../categorySchema/model/CategorySchema';
import { UserSites } from '../../sites/model/UserSites';
import { UserGroup } from '../../userGroup/model/UserGroup';
import { Session } from '../../session/model/Session';

@Table({
  timestamps: true,
  tableName: 'user',
})
export class User extends Model<User> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  email: string;

  @AllowNull(true)
  @Column({type: DataType.STRING})
  password_hash: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  salt: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  first_name: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  last_name: string;

  @AllowNull(true)
  @Column({type: DataType.STRING})
  refresh_token: string;

  @HasMany(() => Price)
  price: Price[];

  @HasMany(() => CategorySchema)
  category_schema: CategorySchema[];

  @HasMany(() => UserSites)
  user_sites: UserSites[];

  @HasMany(() => UserGroup)
  user_group: UserGroup[];

  @HasMany(() => Session)
  session_role: Session[];
}
