import {
	forwardRef,
	Inject,
	Injectable,
	InternalServerErrorException,
} from '@nestjs/common';
import * as C from '../util/constants';
import { CreateUserDto } from './dto/request/create-user.dto';
import { User } from './model/User';
import { UserGroupService } from '../userGroup/userGroup.service';
import { AuthService } from '../auth';
import { Password } from '../util/password';
import { BaseService } from '../common/BaseService';

@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @Inject(C.USER_REPOSITORY)
    public readonly userRepository: typeof User,
    private readonly userGroupService: UserGroupService,
    @Inject(forwardRef(() => AuthService))
    public readonly authService: AuthService,
  ) {super(User); }

  public async registration(createUserDto: CreateUserDto): Promise<User> {
    const passwordData = Password.generatePasswordData(createUserDto.password);
    try {
      const user = await this.create(createUserDto, passwordData);

      await this.userGroupService.searchAndCreate(
				user.dataValues.id,
				'USER',
			);

      return user;
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

  public async checkEmail(email: string): Promise<boolean> {
    try {
      const result = await this.findOne(
        {email},
        ['email'],
      );

      return result !== null;
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }
}