import { ApiModelProperty } from '@nestjs/swagger';
import { TokensDto } from '../../../auth/dto/response/tokens.dto';
import { UserDataDto } from '../user-data.dto';

export class RegistrationDto extends UserDataDto {
  @ApiModelProperty()
  readonly id: number;

  @ApiModelProperty()
  readonly tokens: TokensDto;
}