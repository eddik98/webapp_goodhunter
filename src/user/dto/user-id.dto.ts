import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
export class UserIdDto {
	@ApiModelProperty()
	@IsNotEmpty()
	readonly user_id: number;
}