import {ApiModelProperty} from '@nestjs/swagger';
import {IsEmail, IsString, IsNotEmpty} from 'class-validator';

export class UserDataDto {
  @ApiModelProperty()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @ApiModelProperty()
  @IsString()
  @IsNotEmpty()
  readonly first_name: string;

  @ApiModelProperty()
  @IsString()
  @IsNotEmpty()
  readonly last_name: string;
}