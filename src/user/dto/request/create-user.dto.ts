import { ApiModelProperty } from '@nestjs/swagger';
import { UserDataDto } from '../user-data.dto';
import { IsNotEmpty } from 'class-validator';

export class CreateUserDto extends UserDataDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly password: string;
}