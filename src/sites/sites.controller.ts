import {
	Controller,
	Get,
	NotFoundException,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiCreatedResponse,
} from '@nestjs/swagger';
import { SitesService } from './sites.service';
import { PriceDataDto } from '../price/dto/price-data.dto';
import { UserSites } from './model/UserSites';
import { BaseController } from '../common/BaseController';

@ApiUseTags('sites')
@Controller('sites')
export class SitesController extends BaseController{
	constructor(private readonly sitesService: SitesService)
	{super(); }

	@Get()
	@ApiCreatedResponse({type: PriceDataDto})
	@ApiOperation({title: 'Get all user sites'})
	async findAll(): Promise<UserSites[]> {
		const result = await this.sitesService.findAll({});

		if (!result) {
			throw new NotFoundException();
		}

		return result;
	}

}