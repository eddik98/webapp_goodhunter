import {Module} from '@nestjs/common';
import {SitesService} from './sites.service';
import {sitesProviders} from './sites.provider';
import { SitesController } from './sites.controller';

@Module({
  imports: [],
  controllers: [SitesController],
  providers: [SitesService, ...sitesProviders],
  exports: [SitesService],
})
export class SitesModule {}