import { Inject, Injectable } from '@nestjs/common';
import { UserSites } from './model/UserSites';
import * as Constants from '../util/constants';
import { BaseService } from '../common/BaseService';

@Injectable()
export class SitesService extends BaseService<UserSites>{
  constructor(
  	@Inject(Constants.SITES_REPOSITORY)
		private readonly typesRepository: typeof UserSites)
	{super(UserSites); }
}