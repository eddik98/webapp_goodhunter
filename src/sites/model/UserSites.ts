import { Table, Column, Model, AllowNull, ForeignKey, PrimaryKey, AutoIncrement, DataType } from 'sequelize-typescript';
import { User } from '../../user/model/User';

@Table({
  timestamps: false,
  tableName: 'user_sites',
})
export class UserSites extends Model<UserSites> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  user_id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  url: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  confirmed: number;
}
