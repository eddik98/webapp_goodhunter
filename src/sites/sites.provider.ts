import * as Constants from '../util/constants';
import { UserSites } from './model/UserSites';

export const sitesProviders = [{
  provide: Constants.SITES_REPOSITORY,
  useValue: UserSites,
}];