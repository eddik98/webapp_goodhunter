import {
	Table,
	Column,
	Model,
	AllowNull,
	PrimaryKey,
	AutoIncrement,
	DataType,
	ForeignKey,
} from 'sequelize-typescript';
import { Route } from '../../route/model/Route';
import { Group } from '../../group/model/Group';

@Table({
  timestamps: false,
  tableName: 'route_group',
})
export class RouteGroup extends Model<RouteGroup> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

	@ForeignKey(() => Route)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  route_id: number;

	@ForeignKey(() => Group)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  group_id: number;
}
