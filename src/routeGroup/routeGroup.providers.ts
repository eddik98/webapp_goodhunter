import { RouteGroup } from './model/RouteGroup';
import * as Constants from '../util/constants';

export const routeGroupProviders = [{
    provide: Constants.ROUTE_GROUP_REPOSITORY,
    useValue: RouteGroup,
}];