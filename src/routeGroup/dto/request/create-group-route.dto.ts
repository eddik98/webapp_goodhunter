import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
export class CreateGroupRouteDto {
	@ApiModelProperty()
	@IsNotEmpty()
	readonly route_id: number;

	@ApiModelProperty()
	@IsNotEmpty()
	readonly group_id: number;
}