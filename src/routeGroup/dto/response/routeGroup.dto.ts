import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import { CreateGroupRouteDto } from '../request/create-group-route.dto';
export class RouteGroupDto extends CreateGroupRouteDto{
	@ApiModelProperty()
	@IsNotEmpty()
	readonly id: number;
}