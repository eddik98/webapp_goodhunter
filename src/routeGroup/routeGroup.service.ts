import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../common/BaseService';
import { RouteGroup } from './model/RouteGroup';
import * as Constants from '../util/constants';

@Injectable()
export class RouteGroupService extends BaseService<RouteGroup>{
  constructor(
  	@Inject(Constants.ROUTE_GROUP_REPOSITORY)
		private readonly routeGroupRepository: typeof RouteGroup,
	) {super(RouteGroup); }
}