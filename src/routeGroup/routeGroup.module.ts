import { Module } from '@nestjs/common';
import { RouteGroupService } from './routeGroup.service';
import { routeGroupProviders } from './routeGroup.providers';

@Module({
  imports: [],
  controllers: [],
  providers: [RouteGroupService, ...routeGroupProviders],
  exports: [RouteGroupService],
})
export class RouteGroupModule {}