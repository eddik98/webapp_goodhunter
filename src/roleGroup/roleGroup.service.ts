import { Inject, Injectable } from '@nestjs/common';
import { RoleGroup } from './model/RoleGroup';
import * as Constants from '../util/constants';
import { BaseService } from '../common/BaseService';

@Injectable()
export class RoleGroupService extends BaseService<RoleGroup>{
  constructor(
  	@Inject(Constants.ROLE_GROUP_REPOSITORY)
		private readonly roleGroupRepository?: typeof RoleGroup,
	) {super(RoleGroup); }
}