import { RoleGroup } from './model/RoleGroup';
import * as Constants from '../util/constants';

export const roleGroupProviders = [{
  provide: Constants.ROLE_GROUP_REPOSITORY,
  useValue: RoleGroup,
}];