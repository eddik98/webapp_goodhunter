import { Module } from '@nestjs/common';
import { RoleGroupService } from './roleGroup.service';
import { roleGroupProviders } from './roleGroup.provider';

@Module({
  imports: [],
  controllers: [],
  providers: [RoleGroupService, ...roleGroupProviders],
  exports: [RoleGroupService],
})
export class RoleGroupModule {}