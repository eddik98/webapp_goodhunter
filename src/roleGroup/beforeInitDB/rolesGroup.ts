import { Group } from '../../group/model/Group';
import { Role } from '../../role/model/Role';
import { RoleGroup } from '../model/RoleGroup';
import { RolesGroup } from './defaultRolesGroup';

export async function saveGroups() {
	try {
		for (const item of Object.keys(RolesGroup)) {
			await Group.findOrCreate({
				where: {name: item},
				defaults: {name: item},
			});
		}
	} catch (e) {
		throw new Error(e);
	}
}

export async function saveRelationshipRolesGroup() {
	try {
		for (const item in RolesGroup) {
			const groupId = await Group.findOne({
				where: {
					name: item,
				},
			});

			const rolesGroupData = RolesGroup[item];

			for (const value of rolesGroupData) {
				const roleData = await Role.findOne({
					where: {
						name: value,
					},
				});

				await RoleGroup.findOrCreate({
					where: {role_id: roleData.id, group_id: groupId.id},
					defaults: {role_id: roleData.id, group_id: groupId.id},
				});
			}
		}
	} catch (e) {
		throw new Error(e);
	}
}