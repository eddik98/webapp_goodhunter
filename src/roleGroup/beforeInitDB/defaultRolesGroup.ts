import { Roles } from '../../role/beforeInitDB/defaultRoles';

export const RolesGroup = {
	USER: [
		Roles.USER,
		Roles.PRICE_EDITOR,
		Roles.SESSION_EDITOR,
		Roles.SCHEMAS_EDITOR,
	],
	ADMIN: [
		Roles.USER,
		Roles.ADMIN,
		Roles.MODERATOR,
		Roles.PRICE_EDITOR,
		Roles.SESSION_EDITOR,
		Roles.SCHEMAS_EDITOR,
	],
	MODERATOR: [
		Roles.USER,
		Roles.MODERATOR,
		Roles.PRICE_EDITOR,
		Roles.SESSION_EDITOR,
		Roles.SCHEMAS_EDITOR,
	],
};