import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType, ForeignKey } from 'sequelize-typescript';
import { Role } from '../../role/model/Role';
import { Group } from '../../group/model/Group';

@Table({
  timestamps: false,
  tableName: 'role_group',
})
export class RoleGroup extends Model<RoleGroup> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @ForeignKey(() => Role)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  role_id: number;

  @ForeignKey(() => Group)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  group_id: number;
}
