import { ApiModelProperty } from '@nestjs/swagger';

export class TokensDto {
  @ApiModelProperty()
  readonly refreshToken: string;

  @ApiModelProperty()
  readonly accessToken: string;

  @ApiModelProperty()
  readonly expIn: number;
}