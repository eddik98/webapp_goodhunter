import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class RefreshTokenDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly token: string;
}