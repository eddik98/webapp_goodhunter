import { ApiModelProperty } from '@nestjs/swagger';

export class AuthUserDto {
  @ApiModelProperty()
  readonly email: string;

  @ApiModelProperty()
  readonly password: string;
}