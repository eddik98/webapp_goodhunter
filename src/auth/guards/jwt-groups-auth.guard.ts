import {
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RouteGroup } from '../../routeGroup/model/RouteGroup';
import { Route } from '../../route/model/Route';

@Injectable()
export class JwtGroupsAuthGuard extends AuthGuard('jwt') {
	request: any;

	constructor() {
		super();
	}

	canActivate(context: ExecutionContext) {
		this.request = context.switchToHttp().getRequest();
		return super.canActivate(context);
	}

	// @ts-ignore
	async handleRequest(err, user, info) {
		if (err || !user) {
			throw err || new UnauthorizedException();
		}

		await this.checkRoutesGroup(
			this.request.route.path,
			Object.keys(this.request.route.methods)[0],
			user.groups_id,
			);
		return user;
	}

	async checkRoutesGroup(url: string, method: string, groups_id: []){
		const route = await Route.findOne({
			where: {url, method}, attributes: ['id']});

		if (route === null){
			throw new UnauthorizedException();
		}

		for (const item of groups_id) {
			const routerGroup = await RouteGroup.findAll({
				where: {
					route_id: route.dataValues.id,
					group_id: item,
				},
				attributes: ['id'],
			});

			if (routerGroup.length === 0){
				throw new UnauthorizedException();
			}
		}
	}

}