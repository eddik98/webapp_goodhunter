import { Injectable, Inject, forwardRef, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserService } from '../user';
import { UserGroupService } from '../userGroup/userGroup.service';
import { User } from '../user/model/User';
import { Password } from '../util/password';
import { TokensDto } from './dto/response/tokens.dto';

@Injectable()
export class AuthService {
  constructor(
    public readonly jwtService: JwtService,
    @Inject(forwardRef(() => UserService))
    public readonly userService: UserService,
    public readonly userGroupService: UserGroupService,
  ) {}

  public async createTokens(email: string, userData?: User): Promise<TokensDto> {
    let user = userData;

    if (userData === undefined) {
      user = await this.userService.findOne({
        email,
      });
    }

    const groupsIds = await this.userGroupService.findAll({
			user_id: user.id,
		}, ['group_id'], true);

    const accessPayload: JwtPayload = {
      user_id: user.id,
      email: user.email,
      groups_id: groupsIds.map((value => value.group_id)),
    };

    const refreshPayload: JwtPayload = {
      user_id: user.id,
      email: user.email,
    };

    const accessToken = this.jwtService.sign(accessPayload);
    const refreshToken = this.jwtService.sign(refreshPayload);

    await this.updateUserRefreshToken(user, refreshToken);

    return {refreshToken, accessToken, expIn: 3600};
  }

  public async refreshTokens(token: string): Promise<TokensDto> {
    let verifyToken = null;

    try {
      verifyToken = await this.jwtService.verify(token);
    } catch (e) {
      throw new HttpException(
        'Invalid refresh token',
        HttpStatus.BAD_REQUEST,
      );
    }

    const user = await this.userService.findOne(
      {id: verifyToken.user_id},
      ['refresh_token', 'id', 'email'],
    );

    if (token !== user.refresh_token) {
      throw new HttpException(
        'Invalid refresh token',
        HttpStatus.BAD_REQUEST,
      );
    }

    const newTokens = await this.createTokens('', user);

    await this.updateUserRefreshToken(user, newTokens.refreshToken);

    return newTokens;
  }

  private async updateUserRefreshToken(user: User, refreshToken: string): Promise<void> {
    try {
    	await this.userService.update(
    		{refresh_token: refreshToken},
				{id: user.id},
				);
    } catch (e) {
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async validateUser(payload: JwtPayload): Promise<any> {
    if (!await this.userService.checkEmail(payload.email)) {
      throw new Error('User with such email does not exist!');
    }
  }

  public async checkPassword(password: string, email: string): Promise<boolean> {
    const result = await this.userService.findOne(
      {email},
      ['salt', 'password_hash'],
    );

    const passData = Password.getPasswordHash(password, result.salt);

    return passData.password_hash === result.password_hash;
  }

}