export interface JwtPayload {
  user_id: number;
  email: string;
  groups_id?: any[];
}