import {
	Controller,
	Post,
	Body,
	BadRequestException,
} from '@nestjs/common';

import {
	ApiUseTags,
	ApiOperation,
	ApiCreatedResponse,
} from '@nestjs/swagger';

import { AuthService } from './auth.service';
import { AuthUserDto } from './dto/request/auth-user.dto';
import { RefreshTokenDto } from './dto/request/refresh-token.dto';
import { LogInDto } from './dto/response/log-in.dto';
import { TokensDto } from './dto/response/tokens.dto';
import { BaseController } from '../common/BaseController';

@Controller('auth')
@ApiUseTags('auth')
export class AuthController extends BaseController {
	constructor(private readonly authService: AuthService) {super(); }

	@Post('login')
	@ApiCreatedResponse({type: LogInDto})
	@ApiOperation({title: 'Login user'})
	async logIn(@Body() authUserDto: AuthUserDto): Promise<LogInDto> {
		if (await this.authService.userService.checkEmail(authUserDto.email) &&
			await this.authService.checkPassword(authUserDto.password, authUserDto.email)) {

			const user = await this.authService.userService.findOne(
				{email: authUserDto.email},
				['id', 'first_name', 'last_name', 'email'],
			);
			const tokens = await this.authService.createTokens(authUserDto.email);

			return {
				id: user.id,
				email: user.email,
				first_name: user.first_name,
				last_name: user.last_name,
				tokens,
			};

		} else {
			throw new BadRequestException('Email or password incorrect!');
		}
	}

	@Post('refresh-tokens')
	@ApiCreatedResponse({type: TokensDto})
	@ApiOperation({title: 'Refresh tokens user'})
	async refreshTokens(@Body() refreshTokenDto: RefreshTokenDto): Promise<TokensDto> {
		return await this.authService.refreshTokens(refreshTokenDto.token);
	}
}
