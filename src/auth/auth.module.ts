import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserModule } from '../user';
import { UserGroupModule } from '../userGroup/userGroup.module';
import { JWT_KEY } from '../util/config';

@Module({
  imports: [
    JwtModule.register({
      secretOrPrivateKey: JWT_KEY,
      signOptions: {
        expiresIn: 3600,
      },
    }),
    forwardRef(() => UserModule),
    UserGroupModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}