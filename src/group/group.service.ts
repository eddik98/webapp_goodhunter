import { Inject, Injectable } from '@nestjs/common';
import { Group} from './model/Group';
import { BaseService } from '../common/BaseService';

@Injectable()
export class GroupService extends BaseService<Group>{
  constructor(
  	@Inject('GroupRepository')
		private readonly gropeRepository: typeof Group,
	) {super(Group); }
}