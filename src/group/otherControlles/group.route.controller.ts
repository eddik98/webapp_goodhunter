import {
	Controller,
	Post,
	Param,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiCreatedResponse,
} from '@nestjs/swagger';
import { GroupService } from '../group.service';
import { CreateGroupRouteDto } from '../../routeGroup/dto/request/create-group-route.dto';
import { RouteGroupService } from '../../routeGroup/routeGroup.service';
import { RouteGroupDto } from '../../routeGroup/dto/response/routeGroup.dto';
import { BaseController } from '../../common/BaseController';

@ApiUseTags('group/:group_id/route')
@Controller('group/:group_id/route')
export class GroupRouteController extends BaseController{
	constructor(
		private readonly groupService: GroupService,
		private readonly routeGroupService: RouteGroupService,
	) {super(); }

	@Post(':route_id')
	@ApiOperation({title: 'Adding routes access to groups'})
	@ApiCreatedResponse({type: RouteGroupDto})
	async create(@Param() params: CreateGroupRouteDto): Promise<RouteGroupDto> {
		return await this.routeGroupService.create({
			route_id: params.route_id,
			group_id: params.group_id,
		});
	}
}