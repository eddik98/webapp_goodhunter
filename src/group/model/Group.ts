import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType, HasMany } from 'sequelize-typescript';
import { RoleGroup } from '../../roleGroup/model/RoleGroup';
import { RouteGroup } from '../../routeGroup/model/RouteGroup';
import { UserGroup } from '../../userGroup/model/UserGroup';

@Table({
  timestamps: false,
  tableName: 'group',
})
export class Group extends Model<Group> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

  @HasMany(() => RoleGroup)
  role_group: RoleGroup[];

	@HasMany(() => RouteGroup)
	route_group: RouteGroup[];

	@HasMany(() => UserGroup)
	user_group: UserGroup[];
}
