import { Module} from '@nestjs/common';
import { GroupService } from './group.service';
import { groupProviders } from './group.providers';
import { RouteModule } from '../route/route.module';
import { RouteGroupModule } from '../routeGroup/routeGroup.module';
import { GroupRouteController } from './otherControlles/group.route.controller';

@Module({
  imports: [RouteModule, RouteGroupModule],
  controllers: [GroupRouteController],
  providers: [GroupService, ...groupProviders],
  exports: [GroupService],
})
export class GroupModule {}