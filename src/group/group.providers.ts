import { Group } from './model/Group';
import * as Constants from '../util/constants';

export const groupProviders = [{
    provide: Constants.GROUP_REPOSITORY,
    useValue: Group,
}];