import { UserGroup } from './model/UserGroup';
import * as Constants from '../util/constants';

export const userGroupProviders = [{
  provide: Constants.USER_GROUP_REPOSITORY,
  useValue: UserGroup,
}];