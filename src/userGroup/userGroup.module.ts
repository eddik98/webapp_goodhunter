import { Module} from '@nestjs/common';
import { UserGroupService } from './userGroup.service';
import { userGroupProviders } from './userGroup.provider';
import { GroupModule } from '../group/group.module';

@Module({
  imports: [GroupModule],
  controllers: [],
  providers: [UserGroupService, ...userGroupProviders],
  exports: [UserGroupService],
})
export class UserGroupModule {}