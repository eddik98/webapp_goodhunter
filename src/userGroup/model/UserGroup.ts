import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType, ForeignKey } from 'sequelize-typescript';
import { User } from '../../user/model/User';
import { Group } from '../../group/model/Group';

@Table({
  timestamps: false,
  tableName: 'user_group',
})
export class UserGroup extends Model<UserGroup> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @ForeignKey(() => Group)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  group_id: number;

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  user_id: number;
}
