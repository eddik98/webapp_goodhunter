import {
	Inject,
	Injectable,
	InternalServerErrorException,
} from '@nestjs/common';
import { UserGroup } from './model/UserGroup';
import * as Const from '../util/constants';
import { BaseService } from '../common/BaseService';
import { GroupService } from '../group/group.service';

@Injectable()
export class UserGroupService extends BaseService<UserGroup> {
	constructor(
		@Inject(Const.USER_GROUP_REPOSITORY)
		private readonly userGroupRepository: typeof UserGroup,
		private readonly groupService: GroupService,
	) {super(UserGroup); }

	public async searchAndCreate(user_id: number, groupName: string) {
		try {
			const group = await this.groupService.findOne({name: groupName});

			await this.create({
				group_id: group.id,
				user_id,
			});
		} catch (e) {
			throw new InternalServerErrorException(e);
		}
	}
}