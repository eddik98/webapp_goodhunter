import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class GetSchemaDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly user_id: number;
}