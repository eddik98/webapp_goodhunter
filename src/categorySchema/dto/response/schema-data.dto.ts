
import {ApiModelProperty} from '@nestjs/swagger';

export class SchemaDataDto {
  @ApiModelProperty()
  readonly user_id: number = 0;

  @ApiModelProperty()
  readonly name: string = '';

  @ApiModelProperty()
  readonly schema: string = '';
}