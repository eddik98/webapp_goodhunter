import { Table, Column, Model, AllowNull, HasMany, ForeignKey, PrimaryKey, AutoIncrement, DataType } from 'sequelize-typescript';
import { User } from '../../user/model/User';
import { Price } from '../../price/model/Price';

@Table({
  timestamps: false,
  tableName: 'category_schema',
})
export class CategorySchema extends Model<CategorySchema> {

  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  user_id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  schema: string;

  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  default: number;

  @HasMany(() => Price)
  price: Price[];
}
