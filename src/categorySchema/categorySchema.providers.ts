import { CategorySchema } from './model/CategorySchema';
import * as Constants from '../util/constants';

export const categorySchemaProviders = [{
    provide: Constants.CATEGORY_SCHEMA_REPOSITORY,
    useValue: CategorySchema,
}];