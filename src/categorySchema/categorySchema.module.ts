import { Module } from '@nestjs/common';
import { CategorySchemaService } from './categorySchema.service';
import { categorySchemaProviders } from './categorySchema.providers';

@Module({
  imports: [],
  controllers: [],
  providers: [CategorySchemaService, ...categorySchemaProviders],
  exports: [CategorySchemaService],
})
export class CategorySchemaModule {}