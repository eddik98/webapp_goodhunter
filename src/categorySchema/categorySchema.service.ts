import { Inject, Injectable } from '@nestjs/common';
import { CategorySchema } from './model/CategorySchema';
import { BaseService } from '../common/BaseService';
import * as C from '../util/constants';

@Injectable()
export class CategorySchemaService extends BaseService<CategorySchema> {
  constructor(
    @Inject(C.CATEGORY_SCHEMA_REPOSITORY)
    public readonly categorySchemaRepository: typeof CategorySchema,
  ) {super(CategorySchema); }

}