import { Table, Column, Model, AllowNull, HasMany, ForeignKey, PrimaryKey, AutoIncrement, DataType } from 'sequelize-typescript';
import { PriceItem } from '../../priceItem/model/PriceItem';
import { CategorySchema } from '../../categorySchema/model/CategorySchema';
import { User } from '../../user/model/User';
import { Session } from '../../session/model/Session';
@Table({
  timestamps: false,
  tableName: 'price',
})
export class Price extends Model<Price> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  user_id: number;

  @ForeignKey(() => CategorySchema)
  @AllowNull(false)
  @Column({type: DataType.INTEGER})
  schema_id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

  @HasMany(() => PriceItem)
  price_item: PriceItem[];

  @HasMany(() => Session)
  session: Session[];
}
