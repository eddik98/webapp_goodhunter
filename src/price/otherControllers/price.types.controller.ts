import {
	Controller,
	Get,
	NotFoundException,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiCreatedResponse,
} from '@nestjs/swagger';
import { TypesService } from '../../types/types.service';
import { PriceDataDto } from '../dto/price-data.dto';
import { Types } from '../../types/model/Types';
import { BaseController } from '../../common/BaseController';

@ApiUseTags('price')
@Controller('price')
export class PriceTypesController extends BaseController {
	constructor(private readonly typesService: TypesService) {super(); }

	@Get('types')
	@ApiCreatedResponse({type: PriceDataDto})
	@ApiOperation({title: 'Get all price types'})
	async findAll(): Promise<Types[]> {
		const result = await this.typesService.findAll({});

		if (!result) {
			throw new NotFoundException();
		}

		return result;
	}

}