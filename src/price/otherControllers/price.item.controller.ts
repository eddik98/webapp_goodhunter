import {
	Body,
	Controller,
	HttpStatus,
	Get,
	Res,
	Param,
	Post,
	Delete, NotFoundException,
} from '@nestjs/common';
import {
	ApiOperation,
	ApiUseTags,
	ApiCreatedResponse,
} from '@nestjs/swagger';
import { PriceItemService } from '../../priceItem/priceItem.service';
import { PriceItem } from '../../priceItem/model/PriceItem';
import { PriceIdItemDto } from '../../priceItem/dto/price-id-item.dto';
import { PriceItemDataDto } from '../../priceItem/dto/response/price-item-data.dto';
import { PriceItemIdDto } from '../../priceItem/dto/price-item-id.dto';
import { BaseController } from '../../common/BaseController';

@ApiUseTags('price/:price_id/item')
@Controller('price/:price_id/item')
export class PriceItemController extends BaseController {
	constructor(private readonly priceItemService: PriceItemService) {super(); }

	@Get()
	@ApiCreatedResponse({type: PriceItem})
	@ApiOperation({title: 'Get all price items'})
	async findAllItemsPriceId(@Param() param: PriceIdItemDto): Promise<PriceItem[]> {
		const result = await this.priceItemService.findAll(param);

		if (!result) {
			throw new NotFoundException();
		}

		return result;
	}

	@Get(':id')
	@ApiCreatedResponse({type: PriceItem})
	@ApiOperation({title: 'Get one price item'})
	async findOnePriceItem(@Param() param: PriceItemIdDto): Promise<PriceItem> {
		const result = await this.priceItemService.findOne({
			id: param.id,
		});

		if (!result) {
			throw new NotFoundException();
		}

		return result;
	}

	@Post()
	@ApiCreatedResponse({type: PriceItem})
	@ApiOperation({title: 'Create price item'})
	async create(@Param() id: PriceIdItemDto, @Body() param: PriceItemDataDto): Promise<PriceItem> {
		return await this.priceItemService.create(Object.assign({}, id, param));
	}

	@Delete()
	@ApiCreatedResponse({type: HttpStatus.OK})
	@ApiOperation({title: 'Delete all price items'})
	async deleteAll(@Param() param: PriceIdItemDto, @Res() res): Promise<any> {
		await this.priceItemService.delete({price_id: param.price_id});
		res.status(HttpStatus.OK).send();
	}

	@Delete(':id')
	@ApiCreatedResponse({type: HttpStatus.OK})
	@ApiOperation({title: 'Delete one price item'})
	async deleteOne(@Param() param: PriceItemIdDto, @Res() res): Promise<any> {
		await this.priceItemService.delete({id: param.id});
		res.status(HttpStatus.OK).send();
	}
}