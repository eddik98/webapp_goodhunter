import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty} from 'class-validator';

export class FindAllDto {
  @ApiModelProperty({type: 'number'})
  @IsNotEmpty()
  readonly user_id: number;
}