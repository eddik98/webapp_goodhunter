import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty} from 'class-validator';

export class PriceDataDto {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly schema_id: number;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;
}