import { Price } from './model/Price';
import * as Constants from '../util/constants';

export const priceProviders = [{
    provide: Constants.PRICE_REPOSITORY,
    useValue: Price,
}];