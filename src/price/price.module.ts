import { Module } from '@nestjs/common';
import { PriceService } from './price.service';
import { priceProviders } from './price.providers';
import { PriceTypesController } from './otherControllers/price.types.controller';
import { TypesModule } from '../types/types.module';
import { PriceItemModule } from '../priceItem/priceItem.module';
import { PriceItemController } from './otherControllers/price.item.controller';

@Module({
  imports: [TypesModule, PriceItemModule],
  controllers: [PriceTypesController, PriceItemController],
  providers: [PriceService, ...priceProviders],
  exports: [PriceService],
})
export class PriceModule {}