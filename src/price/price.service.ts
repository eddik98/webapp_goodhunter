import { Inject, Injectable } from '@nestjs/common';
import * as C from '../util/constants';
import { Price } from './model/Price';
import { BaseService } from '../common/BaseService';

@Injectable()
export class PriceService extends BaseService<Price> {
	constructor(
		@Inject(C.PRICE_REPOSITORY)
		public readonly priceRepository: typeof Price,
	) {super(Price); }
}