import { Table, Column, Model, AllowNull, PrimaryKey, AutoIncrement, DataType, HasMany } from 'sequelize-typescript';
import { RoleGroup } from '../../roleGroup/model/RoleGroup';

@Table({
  timestamps: false,
  tableName: 'role',
})
export class Role extends Model<Role> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column({type: DataType.INTEGER})
  id: number;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  name: string;

  @HasMany(() => RoleGroup)
  role_group: RoleGroup[];
}
