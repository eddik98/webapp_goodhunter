import { Module} from '@nestjs/common';
import { RoleService } from './role.service';
import { roleProviders } from './role.providers';
import { RoleGroupModule } from '../roleGroup/roleGroup.module';

@Module({
  imports: [RoleGroupModule],
  controllers: [],
  providers: [RoleService, ...roleProviders],
  exports: [RoleService],
})
export class RoleModule {}