import { Role } from '../model/Role';
import { Roles } from './defaultRoles';

export async function saveRoles() {
	try {
		for (const item in Roles) {
			if (isNaN(Number(item))) {
				await Role.findOrCreate({
					where: {name: item},
					defaults: {name: item},
				});
			}
		}
	} catch (e) {
		throw new Error(e);
	}
}