import { Role } from './model/Role';
import * as Constants from '../util/constants';

export const roleProviders = [{
    provide: Constants.ROLE_REPOSITORY,
    useValue: Role,
}];