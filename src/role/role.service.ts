import { Inject, Injectable } from '@nestjs/common';
import { Role } from './model/Role';
import { BaseService } from '../common/BaseService';

@Injectable()
export class RoleService extends BaseService<Role>{
  constructor(
  	@Inject('RoleRepository')
		private readonly roleRepository: typeof Role,
	) {super(Role); }
}